#Main Menú Principal
from all_class import *

maxCap=int(input("Introduce el número total de plazas: "))
bus1=Bus(maxCap)
plazas_ocupadas = 0
opcion=-1	
while opcion!=0:
		
    print("***********AI Transportes***************\n")
    opcion=int(input("Elige una opción:\n 1. Venta de billetes\n 2. Devolución de billetes\n 3. Estado de la venta\n 0. Salir\n"))
    print("****************************************")
    if opcion==1:
        print("************VENTA DE BILLETES*******************")
        nombre=(input( "Ingrese El Nombre Del Cliente: "))
        apellido=(input( "Ingrese El Apellido Del Cliente: "))
        
        if bus1.add_billete(Billete(nombre,apellido))!= False:
            print("Billete Comprado Con Exito")
        else:
            print("No Quedan Plazas en el Bus Actual")
        
    elif opcion==2:
        print("************DEVOLUCIÓN DE BILLETES*******************")
        nombre=(input( "Ingrese El Nombre Del Cliente: "))
        apellido=(input( "Ingrese El Apellido Del Cliente: "))
        check_billete = bus1.check(nombre,apellido)        
        if check_billete==False:
            print("No hay ningun billete con este Cliente")
        else:
            bus1.dev_billete(check_billete)
            print("Billete Devuelto Con Exito")        
    elif opcion==3:
        print("************ESTADO DE VENTA*******************")
        print(bus1.getInfo())
        print("*********Lista de Pasajeros********")
        for billete in bus1.getBilletes():
            print(billete.get_nombre()+" "+billete.get_apellido())         
    
    elif opcion==0:
        print("Saliendo...")
    else:
        print("Opción no válida.")
